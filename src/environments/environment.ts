// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyAwrLtlmbUJhJiJAY-eH9dJ_qRroV3gCCQ",
    authDomain: "web-page-f2604.firebaseapp.com",
    databaseURL: "https://web-page-f2604.firebaseio.com",
    projectId: "web-page-f2604",
    storageBucket: "web-page-f2604.appspot.com",
    messagingSenderId: "889884657282",
    appId: "1:889884657282:web:9cda2beea3d22bab57969b",
    measurementId: "G-3J9L2ZN4FS"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
