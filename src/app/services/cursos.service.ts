import { Injectable, } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable, Subject } from 'rxjs';
import { map } from 'rxjs/operators';
import { CursosI } from "../models/CursosI";

export interface CursosID extends CursosI { id: string; }

@Injectable({
  providedIn: 'root'
})
export class CursosService {
  private cursosCollection: AngularFirestoreCollection<CursosI>;
 // private cursosDoc: AngularFirestoreDocument<CursosI>;
  cursos: Observable<any>;
  registros:any;

  constructor(private readonly afs:AngularFirestore) {
   // this.getAllCursos();

   }

   getAllCursos(){
    this.cursosCollection = this.afs.collection<CursosI>('cursos');
     this.cursos=this.cursosCollection.snapshotChanges().pipe(
      map(actions => 
        actions.map(a => {
          const data = a.payload.doc.data() as CursosI;
          this.registros=data;
          const id = a.payload.doc.id;
          return { id, ...data };
        }))
      );
   }

  saveCurso(newCurso: CursosI){
    return this.cursosCollection.add(newCurso);
  }

  updateCurso(newCurso: CursosI,data) {
    
   return this.cursosCollection.doc(data.id).update(newCurso);
  }

  deleteCurso(data){
    return this.cursosCollection.doc(data.id).delete();
  }


}
