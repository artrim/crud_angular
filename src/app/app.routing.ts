//Importar modulos de router de angular
import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

//importar componentes
import { CursosComponent } from 'src/app/components/cursos/cursos.component';
import { ContactComponent } from 'src/app/components/contact/contact.component';
//Array  de rutas
const appRoutes: Routes = [

    {path: 'cursos', component: CursosComponent },
    {path: 'contacto', component: ContactComponent },
	{path: '**', component: CursosComponent }

];

//Exportar el modulo de routers
export const appRoutingProviders: any[] = [];
export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);

