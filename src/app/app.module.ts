import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { routing, appRoutingProviders } from './app.routing';


import { ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { ContactComponent } from './components/contact/contact.component';
import { CursosComponent } from './components/cursos/cursos.component';

//bootstrap
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

//firebase
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { environment } from '../environments/environment';

//services
import { DataDbService } from './services/data-db.service';
import { CursosService } from './services/cursos.service';
import { FormcursoComponent } from './components/cursos/formcurso/formcurso.component';

// Material
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatDialogModule } from '@angular/material/dialog'

@NgModule({
  declarations: [
    AppComponent,
    CursosComponent,
    ContactComponent,
    FormcursoComponent
  ],
  imports: [
    BrowserModule,
    routing,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFirestoreModule,
    ReactiveFormsModule,
    NgbModule,
    BrowserAnimationsModule,
    MatDialogModule
  ],
  exports: [
    MatDialogModule,
    FormcursoComponent
  ],
  entryComponents:[
    CursosComponent,
    FormcursoComponent
  ],
  providers: [
    CursosService,
    DataDbService,
    appRoutingProviders],
  bootstrap: [AppComponent]
})
export class AppModule { }
