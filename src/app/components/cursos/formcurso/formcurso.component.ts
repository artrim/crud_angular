import { Component, OnInit,Output,EventEmitter,ViewChild, Inject } from '@angular/core';
import {CursosService} from '../../../services/cursos.service';
import { FormControl, FormGroup, Validators} from '@angular/forms';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';




@Component({
  selector: 'formcurso',
  templateUrl: './formcurso.component.html',
  styleUrls: ['./formcurso.component.css']
})
export class FormcursoComponent implements OnInit {
  typeView: any = 'NUEVO CONTACTO';
  save: any = 'Guardar'
  type: number = 0;
  @Output() registro= new EventEmitter();

  createFormGroup(data) {
    return new FormGroup({
      instructor: new FormControl(data.instructor, [Validators.required, Validators.minLength(8)]),
      curso: new FormControl(data.curso, [Validators.required, Validators.minLength(1)]),
      pagina: new FormControl(data.pagina, [Validators.required, Validators.minLength(10)])
    });
  }
  
  cursosForm: FormGroup;
  constructor(
    private dbCursos: CursosService,
    private modalService: NgbModal,
    public dialogRef: MatDialogRef<FormcursoComponent>,
    @Inject(MAT_DIALOG_DATA) public data
    ) {
    if (data) {
      this.typeView = 'ACTUALIZAR CONTACTO';
      this.save = 'Actualizar'
      this.type = 1;
      this.cursosForm = this.createFormGroup(data);
      console.log(this.cursosForm, data);
    } else {
      this.cursosForm = this.createFormGroup({
        instructor: '',
        curso: '',
        pagina: ''
      });
    }
    
   }

  ngOnInit() {
  }

  onResetCursForm(){
    this.cursosForm.reset();
  }

  onSaveCursForm(){
    if (this.cursosForm.valid) {
      if (this.type === 0){
        this.dbCursos.saveCurso(this.cursosForm.value).then( data => {
          this.cursosForm.reset();
          console.log('inserto con exito');
          this.dialogRef.close({refresh: true});
        }, err => {
          console.log('ocurrio un error');
          
        });
      } else {
        this.dbCursos.updateCurso(this.cursosForm.value,this.data).then( data => {
          this.cursosForm.reset();
          console.log('Actualizado con exito');
          this.dialogRef.close({refresh: true});
        },err => {
          console.log('ocurrio un error al Actualizar');
        });
        this.dialogRef.close({refresh: true});
      }
    }else{
      console.log('Formulario Invalido');
    }
  }


  
  //onClose(){
  // this.dialogRef.close({refresh: false});
  //}
   
   
  clear(){
    this.dialogRef.close({refresh: false});
  }

  get instructor() {return this.cursosForm.get('instructor');}
  get curso() {return this.cursosForm.get('curso');}
  get pagina() {return this.cursosForm.get('pagina');}

}
