import { Component, OnInit,Input,ViewChild } from '@angular/core';
import { CursosI  } from '../../models/CursosI';
import { CursosService } from '../../services/cursos.service';
//import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import{FormcursoComponent}from '../cursos/formcurso/formcurso.component';
import {MatDialog, MatDialogRef} from '@angular/material/dialog';





@Component({
  selector: 'app-cursos',
  templateUrl: './cursos.component.html',
  styleUrls: ['./cursos.component.css']
})
export class CursosComponent implements OnInit {
  cursos: any = [];
  Item: any;
  
  constructor(
    public services: CursosService,
    //private modalService: NgbModal,
    public dialog: MatDialog
    
    ) {

   }
  
  getAll(){
    
    this.services.getAllCursos();
    this.services.cursos.subscribe(
      data=>{
      this.cursos = [];
      data.forEach( (item, index) => {
        this.cursos.push({
          id: item.id,
          numero: index+1,
          instructor: item.instructor,
          pagina: item.pagina,
          curso: item.curso
        })
        
      });
    }, error =>{console.log(error);
    });
    
     
  }


  ver(cursoModal) {
    this.openDialog(0);
  }

  edit(curso) {
    this.openDialog(1,curso);
  }

  delete(){
    this.services.deleteCurso(this.Item);
    this.onclose();
  }

  ValidaModal(validaEliminar,curso): void {
    var nombre: String;
    var asignatura:String;

    this.Item = curso;
    const dialogRef = this.dialog.open(validaEliminar, {
      width: '300px'
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  onclose(){
    this.dialog.closeAll();
  }


  openDialog(type,obj = {}): void { 
    let params = {
      width: '500px'
    };

    if (type === 1) {
      params['data'] = obj;
    }
    const dialogRef = this.dialog.open(FormcursoComponent, params);

    dialogRef.afterClosed().subscribe(result => {
      console.log(result);
      if (result && result.resfresh) {
        console.log('refresh');
        this.getAll();
      }
    });
  }

  ngOnInit() {
    this.cursos=[];
    this.getAll();
  }
/*
  clear(event){
    console.log(event);
    var d=this;
    
    this.services.cursos.subscribe(
      data=>{
      data.forEach( (item, index) => {
        d.cursos.push({
          id: item.id,
          numero: index+1,
          instructor: item.instructor,
          pagina: item.pagina,
          curso: item.curso
        })
      });
    },
    err=>{console.log(err)},
    ()=>{console.log(d.cursos)});
  }*/
 
}
